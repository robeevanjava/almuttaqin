var syshost = window.location.hostname;
var sysport = window.location.port;
if (sysport != ''){
	syshost = syshost+":"+sysport;
}
var syspath = 'https://'+syshost+'/esdm/';


function build_kelurahan_option(idname, kec, kel)
{
    $.get( syspath+"home/get_kelurahan/"+kec, function( data ) {
        $(idname)
            .find('option')
            .remove()
            .end();
        $.each(data, function(i, item){
            var str = data[i].nama;
            str = str.toLowerCase().replace(/\b[a-z]/g, function(letter) {
                return letter.toUpperCase();
            });

            $(idname)
                .append($("<option></option>")
                .attr("value",data[i].id)
                .text(str)); 
        });
        
        if (kel != 0) {
            $(idname).val(kel);
        }
            
    });
}

function urlencode(str) {
    str = escape(str);
    str = str.replace('+', '%2B');
    str = str.replace('%20', '+');
    str = str.replace('*', '%2A');
    str = str.replace('/', '%2F');
    str = str.replace('@', '%40');
    
    return str;
}
function commafy( num ) {
    var str = num.toString().split('.');
    if (str[0].length >= 5) {
        str[0] = str[0].replace(/(\d)(?=(\d{3})+$)/g, '$1,');
    }
    if (str[1] && str[1].length >= 5) {
        str[1] = str[1].replace(/(\d{3})/g, '$1 ');
    }
    return str.join('.');
}
