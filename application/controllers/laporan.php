<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Laporan extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		if($this->session->userdata('masjid_logged') === TRUE)
		{
			$this->gain_access = $this->Menu_model->menu_access();
		}
		else
			redirect( base_url()."login/?next=laporan" );
		date_default_timezone_set("Asia/Jakarta");
		$this->load->model('Laporan_model');
	}
	
	
	public function index()
	{
		if($this->session->userdata('masjid_logged') === TRUE && in_array(3,$this->gain_access))
		{
			$view['menu'] = $this->Menu_model->menu_builder(3,0);
			$view['title'] = 'Laporan';
			$view['co_title'] = '';
			$data['content'] = '';
			$view['content']= $this->load->view('laporan',$data,TRUE);	
			
			$this->Logging_model->SaveLog(__METHOD__, 'Success');
			
			$this->load->view('frame',$view);	
		}else{
			$this->Logging_model->SaveLog(__METHOD__, 'Failed');
			redirect( base_url()."login/?next=laporan" );
		}
	}
	
	public function grid()
	{
		if($this->session->userdata('masjid_logged') === TRUE && in_array(3,$this->gain_access))
		{
			if(isset($_POST['page']))
			{
				$page = intval($_POST['page']);
				$keyword = $_POST['keyword'];
			}else{
				$page = 1;
				$keyword = '';
			}
			$data = $this->Laporan_model->grid($page, $keyword);
			$this->load->view('laporan_grid',$data);	
		}else{
			$this->Logging_model->SaveLog(__METHOD__, 'Failed');
			redirect( base_url()."login/?next=laporan" );
		}
	}

	public function add_form()
	{
		if($this->session->userdata('masjid_logged') === TRUE && in_array(3,$this->gain_access))
		{
			$data['ttd'] = $this->General_model->load_alluser();
			$data['mode'] = 'add';
			$this->load->view('laporan_form',$data);	
		}else{
			$this->Logging_model->SaveLog(__METHOD__, 'Failed');
			redirect( base_url()."login/?next=laporan" );
		}
	}

	public function edit_form($id)
	{
		if($this->session->userdata('masjid_logged') === TRUE && in_array(3,$this->gain_access))
		{
			$data['ttd'] = $this->General_model->load_alluser();
			$data['mode'] = 'edit';
			$data['lap'] = $this->Laporan_model->load_lap($id);
			$this->load->view('laporan_form',$data);	
		}else{
			$this->Logging_model->SaveLog(__METHOD__, 'Failed');
			redirect( base_url()."login/?next=laporan" );
		}
	}

	public function del_form($id)
	{
		if($this->session->userdata('masjid_logged') === TRUE && in_array(3,$this->gain_access))
		{
			$data['lap'] = $this->Laporan_model->load_lap($id);
			$this->load->view('laporan_delform',$data);	
		}else{
			$this->Logging_model->SaveLog(__METHOD__, 'Failed');
			redirect( base_url()."login/?next=laporan" );
		}
	}
	
	public function form_process()
	{
		if($this->session->userdata('masjid_logged') === TRUE && in_array(3,$this->gain_access))
		{
			$val = $this->db->escape(json_encode($_POST));
			$data = $_POST;
			$this->load->model('User_model');
			$userdata = $this->User_model->load_userdata($this->session->userdata('masjid_username'));
			$data['operator'] = $userdata['id'];
			unset($data['mode']);
			if($_POST['mode'] == 'add')
				$ret = $this->Laporan_model->add_process($data);
			else
				$ret = $this->Laporan_model->update_process($data);
			

			$this->Logging_model->SaveLog(__METHOD__, "$val , Success");
			echo $ret;	
		}else{
			echo 'denied';
		}
	}

	public function del_process()
	{
		if($this->session->userdata('masjid_logged') === TRUE && in_array(3,$this->gain_access))
		{
			$val = $this->db->escape(json_encode($_POST));
			$data = $_POST;
			
			$ret = $this->Laporan_model->del_process(md5($data['id']));
			
			$this->Logging_model->SaveLog(__METHOD__, "$val , Success");
			echo $ret;	
		}else{
			echo 'denied';
		}
	}

	public function pdfprint($id)
	{
		if($this->session->userdata('masjid_logged') === TRUE && in_array(3,$this->gain_access))
		{
			$lap = $this->Laporan_model->load_lap($id);

			$dmu = $this->Laporan_model->distinctMatauang();
			foreach($dmu as $row)
			{
				$bef_mu[$row->matauang]['kode'] = $this->General_model->get_matauang($row->matauang)['kode'];
				$bef_mu[$row->matauang]['debet'] = $this->Laporan_model->sumDebetBefore($lap['start'], $row->matauang);
				$bef_mu[$row->matauang]['kredit'] = $this->Laporan_model->sumKreditBefore($lap['start'], $row->matauang);
				$bef_mu[$row->matauang]['saldo'] = $bef_mu[$row->matauang]['debet'] - $bef_mu[$row->matauang]['kredit'];
			}
			
			$judultgl = strtoupper( $this->config->item('hari_ind')[date('N', strtotime($lap['finish']))] .', TANGGAL : '. date('d ', strtotime($lap['finish'])). 
							$this->config->item('bulan_ind')[date('n', strtotime($lap['finish']))] . date(' Y ', strtotime($lap['finish']))
				);

			$this->load->library('Pdf');
			$pdf = new Pdflaporan('P', 'mm', 'A4', true, 'UTF-8', false);
			$pdf->SetCreator(PDF_CREATOR);
			$pdf->SetAuthor('eLaporan');
			$pdf->SetTitle('Laporan Keuangan Masjid');
			
			$pdf->SetMargins(10, 5, 10, 10);
			$pdf->setPrintHeader(false);
			$pdf->setPrintFooter(true);
			
			$pdf->SetAutoPageBreak(TRUE, 15);

			$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
			
			$pdf->AddPage('P');

			$pdf->SetFont('helvetica', '', 10);

			$pdf->SetFont('helvetica', 'B', 12);
			$pdf->MultiCell(0,0,'LAPORAN KEUANGAN '.strtoupper($this->config->item('company')), '', 'C', false, 1, '', '', true, 0, false, true, 8, 'M', false);
			$pdf->MultiCell(0,0,$judultgl, 'B', 'C', false, 1, '', '', true, 0, false, true, 8, 'M', false);

			//$pdf->MultiCell(0,0,'', '', 'C', false, 1, '', '', true, 0, false, true, 8, 'M', false);

			$pdf->SetFont('helvetica', 'B', 9);
			$pdf->MultiCell(140,0,'SALDO KAS TGL. '.strtoupper(date('d ', strtotime($lap['start'])). 
							$this->config->item('bulan_ind')[date('n', strtotime($lap['start']))] . date(' Y ', strtotime($lap['start'])))
							, 'B', 'L', false, 0, '', '', true, 0, false, true, 8, 'M', false);

			$pdf->MultiCell(10,0,'IDR', 'BR', 'R', false, 0, '', '', true, 0, false, true, 8, 'M', false);
			$pdf->MultiCell(30,0,number_format($bef_mu[1]['saldo']), 'B', 'R', false, 1, '', '', true, 0, false, true, 8, 'M', false);


			// -------------------------- PENERIMAAN --------------------------------------------------------------------
			$pdf->SetFont('helvetica', 'B', 9);
			$pdf->MultiCell(0,0,'I. PENERIMAAN', '', 'L', false, 1, '', '', true, 0, false, true, 8, 'M', false);
			$pdf->SetFont('helvetica', '', 9);

			$debets = $this->Laporan_model->debetWith($lap['start'], $lap['finish']);
			foreach ($debets as $dbt) {
				$pdf->MultiCell(140,0,$dbt->keterangan, 'B', 'L', false, 0, '', '', true, 0, false, true, 8, 'M', false);
				$pdf->MultiCell(10,0,$dbt->kode, 'BR', 'R', false, 0, '', '', true, 0, false, true, 8, 'M', false);
				$pdf->MultiCell(30,0,number_format($dbt->nominal), 'B', 'R', false, 1, '', '', true, 0, false, true, 8, 'M', false);
			}

			$pdf->MultiCell(0,0,'', '', 'C', false, 1, '', '', true, 0, false, true, 8, 'M', false);

			$pdf->SetFont('helvetica', 'B', 9);
			foreach($dmu as $row)
			{
				$debetlist = $this->Laporan_model->sumDebetWith($lap['start'], $lap['finish'], $row->matauang);
				$kode = $this->General_model->get_matauang($row->matauang)['kode'];
				if($debetlist > 0)
				{
					$pdf->MultiCell(140,0,'JUMLAH PENERIMAAN', 'B', 'L', false, 0, '', '', true, 0, false, true, 8, 'M', false);
					$pdf->MultiCell(10,0,$kode, 'BR', 'R', false, 0, '', '', true, 0, false, true, 8, 'M', false);
					$pdf->MultiCell(30,0,number_format($debetlist), 'B', 'R', false, 1, '', '', true, 0, false, true, 8, 'M', false);
				}
			}

			$pdf->MultiCell(0,0,'', '', 'C', false, 1, '', '', true, 0, false, true, 8, 'M', false);
			$pdf->MultiCell(0,0,'JUMLAH KAS', '', 'L', false, 1, '', '', true, 0, false, true, 8, 'M', false);

			foreach($dmu as $row)
			{
				if($row->matauang == 1) // rupiah only
				{
					$debetkas = $this->Laporan_model->sumDebetBefore($lap['finish'], $row->matauang);
					$kreditkas = $this->Laporan_model->sumKreditBefore($lap['start'], $row->matauang);
					$kode = $this->General_model->get_matauang($row->matauang)['kode'];

					$saldo = $debetkas - $kreditkas;

					if($saldo > 0)
					{
						$pdf->MultiCell(140,0,'KAS', 'B', 'L', false, 0, '', '', true, 0, false, true, 8, 'M', false);
						$pdf->MultiCell(10,0,$kode, 'BR', 'R', false, 0, '', '', true, 0, false, true, 8, 'M', false);
						$pdf->MultiCell(30,0,number_format($saldo), 'B', 'R', false, 1, '', '', true, 0, false, true, 8, 'M', false);
					}
				}
				
			}

			$pdf->MultiCell(0,0,'', '', 'C', false, 1, '', '', true, 0, false, true, 8, 'M', false);

			// -------------------------- PENGELUARAN --------------------------------------------------------------------
			$pdf->SetFont('helvetica', 'B', 9);
			$pdf->MultiCell(0,0,'II. PENGELUARAN', '', 'L', false, 1, '', '', true, 0, false, true, 8, 'M', false);
			$pdf->SetFont('helvetica', '', 9);

			$kredits = $this->Laporan_model->kreditWith($lap['start'], $lap['finish']);
			foreach ($kredits as $krd) {
				$pdf->MultiCell(140,0,$krd->keterangan, 'B', 'L', false, 0, '', '', true, 0, false, true, 8, 'M', false);
				$pdf->MultiCell(10,0,$krd->kode, 'BR', 'R', false, 0, '', '', true, 0, false, true, 8, 'M', false);
				$pdf->MultiCell(30,0,number_format($krd->nominal), 'B', 'R', false, 1, '', '', true, 0, false, true, 8, 'M', false);
			}

			$pdf->MultiCell(0,0,'', '', 'C', false, 1, '', '', true, 0, false, true, 8, 'M', false);

			$pdf->SetFont('helvetica', 'B', 9);
			foreach($dmu as $row)
			{
				$kreditlist = $this->Laporan_model->sumKreditWith($lap['start'], $lap['finish'], $row->matauang);
				$kode = $this->General_model->get_matauang($row->matauang)['kode'];
				if($kreditlist > 0)
				{
					$pdf->MultiCell(140,0,'JUMLAH PENGELUARAN', 'B', 'L', false, 0, '', '', true, 0, false, true, 8, 'M', false);
					$pdf->MultiCell(10,0,$kode, 'BR', 'R', false, 0, '', '', true, 0, false, true, 8, 'M', false);
					$pdf->MultiCell(30,0,number_format($kreditlist), 'B', 'R', false, 1, '', '', true, 0, false, true, 8, 'M', false);
				}
			}


			$pdf->MultiCell(0,0,'', '', 'C', false, 1, '', '', true, 0, false, true, 8, 'M', false);

			// -------------------------- SALDO KAS --------------------------------------------------------------------
			$pdf->SetFont('helvetica', 'B', 9);
			$pdf->MultiCell(0,0,'III. SALDO KAS '.$judultgl, '', 'L', false, 1, '', '', true, 0, false, true, 8, 'M', false);

			foreach($dmu as $row)
			{
				$debetkas = $this->Laporan_model->sumDebetBefore($lap['finish'], $row->matauang);
				$kreditkas = $this->Laporan_model->sumKreditBefore($lap['finish'], $row->matauang);
				$kode = $this->General_model->get_matauang($row->matauang)['kode'];

				$saldo = $debetkas - $kreditkas;

				if($saldo > 0)
				{
					$pdf->MultiCell(140,0,'', 'B', 'L', false, 0, '', '', true, 0, false, true, 8, 'M', false);
					$pdf->MultiCell(10,0,$kode, 'BR', 'R', false, 0, '', '', true, 0, false, true, 8, 'M', false);
					$pdf->MultiCell(30,0,number_format($saldo), 'B', 'R', false, 1, '', '', true, 0, false, true, 8, 'M', false);
				}
			}

			$pdf->MultiCell(0,0,'', '', 'C', false, 1, '', '', true, 0, false, true, 8, 'M', false);
			// -------------------------- PETUGAS --------------------------------------------------------------------
			$pdf->SetFont('helvetica', 'B', 9);
			$pdf->MultiCell(0,0,'IV. PETUGAS JUMAT HARI INI', '', 'L', false, 1, '', '', true, 0, false, true, 8, 'M', false);
			$pdf->SetFont('helvetica', '', 9);

			$pdf->MultiCell(40,0,'IMAM', '', 'L', false, 0, '', '', true, 0, false, true, 8, 'M', false);
			$pdf->MultiCell(40,0,' : '.$lap['imam'], '', 'L', false, 1, '', '', true, 0, false, true, 8, 'M', false);

			$pdf->MultiCell(40,0,'KHATIB PMB', '', 'L', false, 0, '', '', true, 0, false, true, 8, 'M', false);
			$pdf->MultiCell(40,0,' : '.$lap['khotib1'], '', 'L', false, 1, '', '', true, 0, false, true, 8, 'M', false);

			$pdf->MultiCell(40,0,'KHATIB PENGGANTI', '', 'L', false, 0, '', '', true, 0, false, true, 8, 'M', false);
			$pdf->MultiCell(30,0,' : '.$lap['khotib2'], '', 'L', false, 1, '', '', true, 0, false, true, 8, 'M', false);

			$pdf->MultiCell(40,0,'BILAL', '', 'L', false, 0, '', '', true, 0, false, true, 8, 'M', false);
			$pdf->MultiCell(30,0,' : '.$lap['bilal'], '', 'L', false, 1, '', '', true, 0, false, true, 8, 'M', false);

			$pdf->MultiCell(40,0,'PROTOKOL', '', 'L', false, 0, '', '', true, 0, false, true, 8, 'M', false);
			$pdf->MultiCell(30,0,' : '.$lap['protokol'], '', 'L', false, 1, '', '', true, 0, false, true, 8, 'M', false);

			// -------------------------- PETUGAS --------------------------------------------------------------------
			$pdf->SetFont('helvetica', 'B', 9);
			$pdf->MultiCell(0,0,'V. PENGUMUMAN', '', 'L', false, 1, '', '', true, 0, false, true, 8, 'M', false);
			$pdf->SetFont('helvetica', '', 9);
			$pdf->MultiCell(0,0,$lap['pengumuman'], '', 'L', false, 1, '', '', true, 0, false, true, 8, 'M', false);

			$pdf->MultiCell(0,0,'', '', 'C', false, 1, '', '', true, 0, false, true, 8, 'M', false);

			$ttd = $this->General_model->userdata($lap['ttd']);
			$pdf->MultiCell(0,0,strtoupper($ttd['jabatan'].'<br>'.$this->config->item('company').'<br><br><br><br><br><b>'.$ttd['nama'].'</b>'), '', 'C', false, 1, '', '', true, 0, true, true, 8, 'M', false);

			$pdf->Output('LaporanKeuanganMasjid'.$id.'.pdf', 'I');
		}else{
			$this->Logging_model->SaveLog(__METHOD__, 'Failed');
			redirect( base_url()."login/?next=laporan" );
		}
	}
}
