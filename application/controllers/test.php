<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Test extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set("Asia/Jakarta");
	}
	
	
	public function index()
	{
		$data['title'] = 'Test';
		$data['co_title'] = 'Test';
		$view['menu'] = $this->Menu_model->menu_builder(1,0);
		$data['content'] = '';
		$view['content']= $this->load->view('dashboard',$data,TRUE);	
		$this->load->view('frame',$view);	
	}
}
