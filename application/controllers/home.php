<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Home extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		if($this->session->userdata('masjid_logged') === TRUE)
		{
			$this->gain_access = $this->Menu_model->menu_access();
		}
		else
			redirect( base_url()."login/?next=home" );
		date_default_timezone_set("Asia/Jakarta");
	}
	
	
	public function index()
	{
		if($this->session->userdata('masjid_logged') === TRUE && in_array(1,$this->gain_access))
		{
			$view['menu'] = $this->Menu_model->menu_builder(1,0);
			$view['title'] = 'Home';
			$view['co_title'] = 'Selamat Datang!';
			$data['content'] = '';
			$view['content']= $this->load->view('home',$data,TRUE);	
			
			$this->Logging_model->SaveLog(__METHOD__, 'Success');
			
			$this->load->view('frame',$view);	
		}else{
			$this->Logging_model->SaveLog(__METHOD__, 'Failed');
			redirect( base_url()."login/?next=home" );
		}
	}
	
	
}
