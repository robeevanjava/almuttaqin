<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Transaksi extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		if($this->session->userdata('masjid_logged') === TRUE)
		{
			$this->gain_access = $this->Menu_model->menu_access();
		}
		else
			redirect( base_url()."login/?next=transaksi" );
		date_default_timezone_set("Asia/Jakarta");
		$this->load->model('Transaksi_model');
	}
	
	
	public function index()
	{
		if($this->session->userdata('masjid_logged') === TRUE && in_array(2,$this->gain_access))
		{
			$view['menu'] = $this->Menu_model->menu_builder(2,0);
			$view['title'] = 'Transaksi';
			$view['co_title'] = '';
			$data['content'] = '';
			$view['content']= $this->load->view('transaksi',$data,TRUE);	
			
			$this->Logging_model->SaveLog(__METHOD__, 'Success');
			
			$this->load->view('frame',$view);	
		}else{
			$this->Logging_model->SaveLog(__METHOD__, 'Failed');
			redirect( base_url()."login/?next=transaksi" );
		}
	}
	
	public function grid()
	{
		if($this->session->userdata('masjid_logged') === TRUE && in_array(2,$this->gain_access))
		{
			if(isset($_POST['page']))
			{
				$page = intval($_POST['page']);
				$keyword = $_POST['keyword'];
			}else{
				$page = 1;
				$keyword = '';
			}
			$data = $this->Transaksi_model->grid($page, $keyword);
			$this->load->view('transaksi_grid',$data);	
		}else{
			$this->Logging_model->SaveLog(__METHOD__, 'Failed');
			redirect( base_url()."login/?next=transaksi" );
		}
	}

	public function add_form()
	{
		if($this->session->userdata('masjid_logged') === TRUE && in_array(2,$this->gain_access))
		{
			$data['matauang'] = $this->General_model->matauang();
			$data['mode'] = 'add';
			$this->load->view('transaksi_form',$data);	
		}else{
			$this->Logging_model->SaveLog(__METHOD__, 'Failed');
			redirect( base_url()."login/?next=transaksi" );
		}
	}

	public function edit_form($id)
	{
		if($this->session->userdata('masjid_logged') === TRUE && in_array(2,$this->gain_access))
		{
			$data['matauang'] = $this->General_model->matauang();
			$data['mode'] = 'edit';
			$data['trx'] = $this->Transaksi_model->load_trx($id);
			$this->load->view('transaksi_form',$data);	
		}else{
			$this->Logging_model->SaveLog(__METHOD__, 'Failed');
			redirect( base_url()."login/?next=transaksi" );
		}
	}

	public function del_form($id)
	{
		if($this->session->userdata('masjid_logged') === TRUE && in_array(2,$this->gain_access))
		{
			$data['trx'] = $this->Transaksi_model->load_trx($id);
			$this->load->view('transaksi_delform',$data);	
		}else{
			$this->Logging_model->SaveLog(__METHOD__, 'Failed');
			redirect( base_url()."login/?next=transaksi" );
		}
	}
	
	public function form_process()
	{
		if($this->session->userdata('masjid_logged') === TRUE && in_array(2,$this->gain_access))
		{
			$val = $this->db->escape(json_encode($_POST));
			$data = $_POST;
			$this->load->model('User_model');
			$userdata = $this->User_model->load_userdata($this->session->userdata('masjid_username'));
			$data['operator'] = $userdata['id'];
			unset($data['mode']);
			if($_POST['mode'] == 'add')
				$ret = $this->Transaksi_model->add_process($data);
			else
				$ret = $this->Transaksi_model->update_process($data);
			

			$this->Logging_model->SaveLog(__METHOD__, "$val , Success");
			echo $ret;	
		}else{
			echo 'denied';
		}
	}

	public function del_process()
	{
		if($this->session->userdata('masjid_logged') === TRUE && in_array(2,$this->gain_access))
		{
			$val = $this->db->escape(json_encode($_POST));
			$data = $_POST;
			
			$ret = $this->Transaksi_model->del_process(md5($data['id']));
			
			$this->Logging_model->SaveLog(__METHOD__, "$val , Success");
			echo $ret;	
		}else{
			echo 'denied';
		}
	}
}
