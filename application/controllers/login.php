<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller {
	public function __construct()
    {
        parent::__construct();
                
    }
    public function index()
    {
        $this->session->sess_destroy();
        $this->load->view("login");
    }
    
    public function process()
    {
        if ($_POST['user_login'])
        {
                $this->load->model('User_model');
                $num = $this->User_model->check_credentials(); 
                if ($this->User_model->check_credentials() == 0)
                {
                        redirect( base_url()."login/?failed=yes" );
                }else{
                        $this->session->set_userdata('masjid_username', $_POST['user_name']);
                        $this->session->set_userdata('masjid_tokenuser', md5($_POST['user_name']) );
                        $this->session->set_userdata('masjid_logged', TRUE);
                        $userdata = $this->User_model->load_userdata($_POST['user_name']);
                        
                        $this->session->set_userdata('masjid_chk', md5($userdata['usergroup']) );
                        
                        if ($_POST['next'] == '')
                                redirect( base_url()."home/" );
                        else
                                redirect( base_url().$_POST['next'] );
                }	
        }else{
                redirect( base_url()."login/?failed=yes" );
        }
    }
}