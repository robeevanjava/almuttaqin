<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once dirname(__FILE__) . '/tcpdf/tcpdf.php';

class Pdf extends TCPDF
{
    function __construct()
    {
        parent::__construct();
    }
}

class PdfRekom extends TCPDF
{
    function __construct()
    {
        parent::__construct();
    }
    
}

class Pdflaporan extends TCPDF
{
    function __construct()
    {
        parent::__construct();
    }
    
    // Page footer
    public function Footer() {
        // Position at 15 mm from bottom
        $this->SetLineStyle( array( 'width' => 0.2, 'color' => array(0, 0, 0)));
        $this->SetY($this->getPageHeight()-10);
        // Set font
        $this->SetFont('helvetica', '', 6);
        
        $this->MultiCell(0,0,'Halaman '.$this->getAliasNumPage().'/'.$this->getAliasNbPages(), 'T', 'C', false, 1, '', '', 1, 0, true, true);
    }
}
