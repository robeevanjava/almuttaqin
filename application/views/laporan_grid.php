<div class="row">
	<div class="col-xs-12">
		<div class="box">
			<div class="box-header">
				<div class="box-title">
					<a class="btn btn-primary" data-toggle="modal" data-target="#myModal" href="<?php echo $this->config->item('base_url')?>laporan/add_form">
					 	<i class="fa fa-check-square"></i> Tambah
					</a>
				</div>
				<div class="box-tools">
					<div class="input-group">
						<input type="text" name="keyword" id="keyword" class="form-control input-sm pull-right" style="width: 150px;" placeholder="Search" value="<?php if(isset($_POST['keyword'])) echo $_POST['keyword'];?>"/>
						<div class="input-group-btn">
							<button class="btn btn-sm btn-default" id="searchBtn"><i class="fa fa-search"></i></button>
						</div>
					</div>
				</div>
			</div><!-- /.box-header -->
			<div class="box-body table-responsive no-padding">
				<table class="table table-hover">
					<tr>
						<th>ID</th>
						<th>Start</th>
						<th>Finish</th>
						<th>Imam</th>
						<th>Khotib PMB</th>
						<th>Khotib Pengganti</th>
						<th>Operator</th>
						<th>Opsi</th>
					</tr>
					<?php
						if ($count <= 0)
						{
					?>
						<tr>
							<td colspan="8">No Data</td>
						</tr>
					<?php
						}else{
							$tipe = $this->config->item('penyalur');
							foreach($list as $row)
							{
					?>
						<tr>
							<td><?php echo $row->id;?></td>
							<td><?php echo $row->start;?></td>
							<td><?php echo $row->finish;?></td>
							<td><?php echo $row->imam;?></td>
							<td><?php echo $row->khotib1;?></td>
							<td><?php echo $row->khotib2;?></td>
							<td><?php echo $row->nama;?></td>
							
							<td>
								<a href="<?php echo $this->config->item('base_url')?>laporan/edit_form/<?php echo md5($row->id);?>/" class="btn btn-xs btn-success" data-toggle="modal" data-target="#myModal">
									<i class="glyphicon glyphicon-pencil"></i> Ubah
								</a>
								<a href="<?php echo $this->config->item('base_url')?>laporan/del_form/<?php echo md5($row->id);?>/" class="btn btn-xs btn-danger" data-toggle="modal" data-target="#myModal">
									<i class="glyphicon glyphicon-remove"></i> Hapus
								</a>
								<a href="<?php echo $this->config->item('base_url')?>laporan/pdfprint/<?php echo md5($row->id);?>/" class="btn btn-xs btn-info" target="_blank">
									<i class="glyphicon glyphicon-book"></i> Cetak
								</a>
							</td>
						</tr>
					<?php
							}
						}
					?>
				</table>
			</div><!-- /.box-body -->
            <div class="box-footer clearfix">
            	<p class="pull-left">Hal : <?php echo $page;?> dari <?php echo $totalpage;?></p>
                <ul class="pagination pagination-sm no-margin pull-right">
                    <li><a href="#" id="prevBtn">&laquo;</a></li>
                    <li><a href="#" id="nextBtn">&raquo;</a></li>
                </ul>
				<input type="hidden" id="totalpage" name="totalpage" value="<?php echo $totalpage;?>">
				<input type="hidden" id="page" name="page" value="<?php echo $page;?>">
            </div>
        </div><!-- /.box -->
    </div>
</div>
<script type="text/javascript">
    $(function() {
		$("#firstBtn").click(function() {
			goToPage(1);
		});
		$("#prevBtn").click(function() {
			var page = parseInt($("#page").val());
			if (page > 1) {
				goToPage(page - 1);
			}else{
				goToPage(1);
			}
		});
		$("#nextBtn").click(function() {
			var totalpage = parseInt( $("#totalpage").val() );
			var page = parseInt($("#page").val());
			if (totalpage > page){
				goToPage(page + 1);
			}else{
				goToPage(totalpage);
			}
		});
		$("#lastBtn").click(function() {
			var totalpage = $("#totalpage").val();
			goToPage(totalpage);
		});
		$("#searchBtn").click(function() {
			goToPage(1);
		});
    });
	
    
</script>