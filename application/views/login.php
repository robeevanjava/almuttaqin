<!DOCTYPE html>
<html class="bg-black">
    <head>
        <meta charset="UTF-8">
        <title><?php echo $this->config->item('title');?></title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <!-- bootstrap 3.0.2 -->
        <link href="<?php echo $this->config->item('base_url');?>style/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <!-- font Awesome -->
        <link href="<?php echo $this->config->item('base_url');?>style/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <!-- Theme style -->
        <link href="<?php echo $this->config->item('base_url');?>style/css/AdminLTE.css" rel="stylesheet" type="text/css" />

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
    </head>
    <body class="bg-black">
		<?php
			$next = '';
			if (isset($_GET['next']))
				$next = $_GET['next'];
		?>
        <div class="form-box" id="login-box">
            <div class="header"><?php echo $this->config->item('appname');?></div>
            <form action="<?php echo $this->config->item('base_url')?>login/process" method="post">
                <div class="body bg-gray">
                    <div class="form-group">
                        <input type="text" name="user_name" class="form-control" placeholder="User ID"/>
                    </div>
                    <div class="form-group">
                        <input type="password" name="user_pass" class="form-control" placeholder="Password"/>
			            <input type="hidden" value="<?php echo $next;?>" id="next" name="next">
                    </div>          
                </div>
                <div class="footer">                                                               
                    <button type="submit" class="btn bg-olive btn-block" name="user_login" value="Masuk">Masuk</button>
		    <div class="margin text-center">
			<?php
			    if (isset($_GET['failed']))
			    echo "<span>Gagal Login: Username atau Password Salah!</span>";
			?>
	
		    </div>
                </div>
            </form>

            
        </div>


        <!-- jQuery 2.0.2 -->
        <script src="<?php echo $this->config->item('base_url');?>style/js/jquery.min.js"></script>
        <!-- Bootstrap -->
        <script src="<?php echo $this->config->item('base_url');?>style/js/bootstrap.min.js" type="text/javascript"></script>

    </body>
</html>