<div class="row">
    <div class="col-xs-12">
		<div class="alert alert-info" id="alertInfo">
			<i class="fa fa-info"></i>
			<div id="alertContent"></div>
		</div>
	</div>
</div>
<div id="ppGrid">

</div>
<script type="text/javascript">
        $(function() {
			$("#ppGrid").html('Load..').load('<?php echo $this->config->item('base_url')?>laporan/grid');
			$("#alertInfo").hide();
                
        });
        
        function goToPage(page){
		var keyword = $("#keyword").val();		
		$.ajax({
			type: "POST",
			url: '<?php echo $this->config->item('base_url')?>laporan/grid',
			data: {page: page, keyword: keyword},
			cache: false,
			success: function(html){
				$('#ppGrid').html(html);
			}
		});	
	}
</script>