<div class="modal-dialog">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			<h4 class="modal-title">Form Hapus Transaksi</h4>
		</div>
		<div class="modal-body">
			<div class="callout callout-danger">
				<h4>Warning!</h4>
				<p>Yakin akan menghapus data transaksi ini? Data yang dihapus tidak dapat dikembalikan/</p>
			</div>
			<dl class="dl-horizontal">
				<dt>Tanggal</dt>
				<dd><?php echo $trx['tanggal']?></dd>
				<dt>Keterangan</dt>
				<dd><?php echo $trx['keterangan']?></dd>
				<dt>Nominal</dt>
				<dd><?php echo $trx['kode'].' '.number_format($trx['nominal'])?></dd>
			</dl>
			<form id="trxdelform" role="form">
				<input type="hidden" id="id" name="id" value="<?php echo (isset($trx)) ? $trx['id'] : '';?>">
			</form>
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Tutup</button>
			<button type="button" class="btn btn-danger btn-sm" id="TrxDelBtn">Hapus</button>
		</div>
	</div><!-- /.modal-content -->
</div><!-- /.modal-dialog -->
<script type="text/javascript">
	$(function() {
		$("[data-mask]").inputmask();
		$("[tanggalan]").datepicker({
                "dateFormat" : 'yy-mm-dd'
        });
		$("#TrxDelBtn").click(function() {
			var element = $(this);
			
			$.ajax({
			type: "POST",
			url: "<?php echo $this->config->item('base_url')?>transaksi/del_process/",
			data: $('#trxdelform').serialize(),
			cache: false,
			success: function(html){
						if (html == '1') {
							$("#ppGrid").html('Load..').load('<?php echo $this->config->item('base_url')?>transaksi/grid');
							$("#alertContent").html('Terhapus');
							$("#alertInfo").show();
							$('#myModal').modal('hide');
						}else{
							alert(html);
						}
                    }
			});
			
			return false;
		});
	});
</script>