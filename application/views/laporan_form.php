<div class="modal-dialog">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			<h4 class="modal-title">Form Laporan</h4>
		</div>
		<div class="modal-body">
			<form id="lapform" role="form">
                <div class="form-group">
					<label for="start">Dari Tanggal</label>
					<input type="text" class="form-control" id="start" name="start" maxlength="10" value="<?php echo (isset($lap) ? $lap['start'] : '');?>" tanggalan>
				</div>
				<div class="form-group">
					<label for="finish">Sampai Tanggal</label>
					<input type="text" class="form-control" id="finish" name="finish" maxlength="10" value="<?php echo (isset($lap) ? $lap['finish'] : '');?>" tanggalan>
				</div>

				<div class="form-group">
					<label for="imam">Imam</label>
					<input type="text" class="form-control" id="imam" name="imam" value="<?php echo (isset($lap)) ? $lap['imam'] : '';?>" maxlength="200">
				</div>
				<div class="form-group">
					<label for="khotib1">Khotib PMB</label>
					<input type="text" class="form-control" id="khotib1" name="khotib1" value="<?php echo (isset($lap)) ? $lap['khotib1'] : '';?>" maxlength="200">
				</div>
				<div class="form-group">
					<label for="khotib2">Khotib Pengganti</label>
					<input type="text" class="form-control" id="khotib2" name="khotib2" value="<?php echo (isset($lap)) ? $lap['khotib2'] : '';?>" maxlength="200">
				</div>
				<div class="form-group">
					<label for="bilal">Bilal</label>
					<input type="text" class="form-control" id="bilal" name="bilal" value="<?php echo (isset($lap)) ? $lap['bilal'] : '';?>" maxlength="200">
				</div>
				<div class="form-group">
					<label for="protokol">Protokol</label>
					<input type="text" class="form-control" id="protokol" name="protokol" value="<?php echo (isset($lap)) ? $lap['protokol'] : '';?>" maxlength="200">
				</div>

				<div class="form-group">
					<label for="pengumuman">Pengumuman</label>
					<textarea class="form-control" id="pengumuman" name="pengumuman"><?php echo (isset($lap)) ? $lap['pengumuman'] : '';?></textarea>
				</div>

				<div class="form-group">
					<label for="ttd">Tandatangan</label>
					<select class="form-control" id="ttd" name="ttd">
						<?php
							foreach($ttd as $mu)
							{
								$selected = (isset($lap)) ? (($lap['ttd'] == $mu->id) ? 'selected=selected' : '') : '';
								echo "<option value=\"".$mu->id."\" ".$selected.">".$mu->nama."</option>";
							}
						?>
					</select>
				</div>

				<input type="hidden" id="id" name="id" value="<?php echo (isset($lap)) ? $lap['id'] : '';?>">
				<input type="hidden" id="mode" name="mode" value="<?php echo $mode;?>">
			</form>
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Tutup</button>
			<button type="button" class="btn btn-primary btn-sm" id="lapBtn">Simpan</button>
		</div>
	</div><!-- /.modal-content -->
</div><!-- /.modal-dialog -->
<script type="text/javascript">
	$(function() {
		$("[data-mask]").inputmask();

		$( "#start" ).datepicker({
			"dateFormat" : 'yy-mm-dd',
			defaultDate: "+1w",
			changeMonth: true,
			numberOfMonths: 3,
			onClose: function( selectedDate ) {
				$( "#finish" ).datepicker( "option", "minDate", selectedDate );
			}
		});
		$( "#finish" ).datepicker({
			"dateFormat" : 'yy-mm-dd',
			defaultDate: "+1w",
			changeMonth: true,
			numberOfMonths: 3,
			onClose: function( selectedDate ) {
				$( "#start" ).datepicker( "option", "maxDate", selectedDate );
			}
		});

		$("#lapBtn").click(function() {
			var element = $(this);
			var start = $("#start").val();
			var finish = $("#finish").val();
                
			if(start == '' || start == '0000-00-00' || finish == '0000-00-00' || finish == '')
			{
				alert("Tolong isi tanggal mulai dan akhir");
			}else{
				$.ajax({
				type: "POST",
				url: "<?php echo $this->config->item('base_url')?>laporan/form_process/",
				data: $('#lapform').serialize(),
				cache: false,
				success: function(html){
							if (html == '1') {
								$("#ppGrid").html('Load..').load('<?php echo $this->config->item('base_url')?>laporan/grid');
								$("#alertContent").html('Saved');
								$("#alertInfo").show();
								$('#myModal').modal('hide');
							}else{
								alert(html);
							}
                        }
				});
			}
			
			return false;
		});
	});
</script>