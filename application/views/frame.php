<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title><?php echo $this->config->item('title');?></title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <!-- bootstrap 3.0.2 -->
        <link href="<?php echo $this->config->item('base_url');?>style/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <!-- font Awesome -->
        <link href="<?php echo $this->config->item('base_url');?>style/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <!-- Ionicons -->
        <link href="<?php echo $this->config->item('base_url');?>style/css/ionicons.min.css" rel="stylesheet" type="text/css" />
        <!-- Morris chart -->
        <link href="<?php echo $this->config->item('base_url');?>style/css/morris/morris.css" rel="stylesheet" type="text/css" />
        <!-- jvectormap -->
        <link href="<?php echo $this->config->item('base_url');?>style/css/jvectormap/jquery-jvectormap-1.2.2.css" rel="stylesheet" type="text/css" />
        <!-- fullCalendar -->
        <link href="<?php echo $this->config->item('base_url');?>style/css/fullcalendar/fullcalendar.css" rel="stylesheet" type="text/css" />
        <!-- Daterange picker -->
        <link href="<?php echo $this->config->item('base_url');?>style/css/daterangepicker/daterangepicker-bs3.css" rel="stylesheet" type="text/css" />
        <!-- bootstrap wysihtml5 - text editor -->
        <link href="<?php echo $this->config->item('base_url');?>style/css/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css" rel="stylesheet" type="text/css" />
        <!-- Theme style -->
        <link href="<?php echo $this->config->item('base_url');?>style/css/AdminLTE.css" rel="stylesheet" type="text/css" />
	
	<link href="<?php echo $this->config->item('base_url');?>style/css/jQueryUI/jquery-ui-1.10.4.custom.min.css" rel="stylesheet" type="text/css" />
	<style>
	    .ui-autocomplete { position: absolute; cursor: default;z-index:5000 !important;}
	    .ui-menu .ui-menu-item a{
		color: #000;
		border-radius: 0px;
		border: 1px solid #454545;
	    }
	</style>
		<!-- jQuery 2.0.2 -->
        <script src="<?php echo $this->config->item('base_url');?>style/js/jquery.min.js"></script>

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
    </head>
    <body class="skin-blue">
        <!-- header logo: style can be found in header.less -->
        <header class="header">
            <a href="<?php echo $this->config->item('base_url');?>" class="logo">
                <!-- Add the class icon to your logo image or logo icon to add the margining -->
                <?php echo $this->config->item('appname');?>
            </a>
            <!-- Header Navbar: style can be found in header.less -->
            <nav class="navbar navbar-static-top" role="navigation">
                <!-- Sidebar toggle button-->
                <a href="#" class="navbar-btn sidebar-toggle" data-toggle="offcanvas" role="button">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </a>
                <div class="navbar-right">
                    <ul class="nav navbar-nav">
                        <li class="dropdown notifications-menu" id="notifbar">
                            
                        </li>
                        <!-- User Account: style can be found in dropdown.less -->
                        <li class="dropdown user user-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="glyphicon glyphicon-user"></i>
                                <span><?php echo $this->session->userdata('masjid_username'); ?><i class="caret"></i></span>
                            </a>
                            <ul class="dropdown-menu">
                                <!-- User image -->
                                <li class="user-header bg-green">
                                    <img src="<?php echo $this->config->item('base_url');?>style/img/icon-mosque.png" class="img-circle" alt="User Image" />
                                    <p>
                                        <?php echo $this->session->userdata('masjid_username'); ?>
                                    </p>
                                </li>
                                <!-- Menu Footer-->
                                <li class="user-footer">
                                    <div class="pull-left">
                                        <a href="<?php echo $this->config->item('base_url');?>user/profile/" class="btn btn-default btn-flat">Reset Password</a>
                                    </div>
                                    <div class="pull-right">
                                        <a href="<?php echo $this->config->item('base_url');?>login/" class="btn btn-default btn-flat">Sign out</a>
                                    </div>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>
        <div class="wrapper row-offcanvas row-offcanvas-left">
            <!-- Left side column. contains the logo and sidebar -->
            <aside class="left-side sidebar-offcanvas">
                <!-- sidebar: style can be found in sidebar.less -->
                <section class="sidebar">
                    <!-- Sidebar user panel -->
                    <div class="user-panel">
                        <div class="pull-left image">
                            <img src="<?php echo $this->config->item('base_url');?>style/img/icon-mosque.png" class="img-circle" alt="User Image" />
                        </div>
                        <div class="pull-left info">
                            <p>Hello, <?php echo $this->session->userdata('masjid_username'); ?></p>
                        </div>
                    </div>
                    
                    <!-- sidebar menu: : style can be found in sidebar.less -->
                    <ul class="sidebar-menu">
                        <?php echo $menu;?>
                    </ul>
                </section>
                <!-- /.sidebar -->
            </aside>

            <!-- Right side column. Contains the navbar and content of the page -->
            <aside class="right-side">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        <?php echo $title;?>
						<small><?php echo $co_title;?></small>
                    </h1>
                </section>

                <!-- Main content -->
                <section class="content">
                    <?php echo $content;?>
					<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
					
					</div>
                </section><!-- /.content -->
            </aside><!-- /.right-side -->
        </div><!-- ./wrapper -->

        <!-- add new calendar event modal -->


        
        <!-- jQuery UI 1.10.3 -->
        <script src="<?php echo $this->config->item('base_url');?>style/js/jquery-ui-1.10.4.min.js" type="text/javascript"></script>
        <!-- Bootstrap -->
        <script src="<?php echo $this->config->item('base_url');?>style/js/bootstrap.min.js" type="text/javascript"></script>
        <!-- Morris.js charts -->
        <script src="<?php echo $this->config->item('base_url');?>style/js/raphael-min.js"></script>
        <script src="<?php echo $this->config->item('base_url');?>style/js/plugins/morris/morris.min.js" type="text/javascript"></script>
        <!-- Sparkline -->
        <script src="<?php echo $this->config->item('base_url');?>style/js/plugins/sparkline/jquery.sparkline.min.js" type="text/javascript"></script>
        <!-- jvectormap -->
        <script src="<?php echo $this->config->item('base_url');?>style/js/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js" type="text/javascript"></script>
        <script src="<?php echo $this->config->item('base_url');?>style/js/plugins/jvectormap/jquery-jvectormap-world-mill-en.js" type="text/javascript"></script>
        <!-- fullCalendar -->
        <script src="<?php echo $this->config->item('base_url');?>style/js/plugins/fullcalendar/fullcalendar.min.js" type="text/javascript"></script>
        <!-- jQuery Knob Chart -->
        <script src="<?php echo $this->config->item('base_url');?>style/js/plugins/jqueryKnob/jquery.knob.js" type="text/javascript"></script>
        <!-- InputMask -->
        <script src="<?php echo $this->config->item('base_url');?>style/js/plugins/input-mask/jquery.inputmask.js" type="text/javascript"></script>
        <script src="<?php echo $this->config->item('base_url');?>style/js/plugins/input-mask/jquery.inputmask.date.extensions.js" type="text/javascript"></script>
	<script src="<?php echo $this->config->item('base_url');?>style/js/plugins/input-mask/jquery.inputmask.numeric.extensions.js" type="text/javascript"></script>
        <script src="<?php echo $this->config->item('base_url');?>style/js/plugins/input-mask/jquery.inputmask.extensions.js" type="text/javascript"></script>
        
        <!-- daterangepicker -->
        <script src="<?php echo $this->config->item('base_url');?>style/js/plugins/daterangepicker/daterangepicker.js" type="text/javascript"></script>
        <!-- Bootstrap WYSIHTML5 -->
        <script src="<?php echo $this->config->item('base_url');?>style/js/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js" type="text/javascript"></script>
        <!-- iCheck -->
        <script src="<?php echo $this->config->item('base_url');?>style/js/plugins/iCheck/icheck.min.js" type="text/javascript"></script>
	<script src="<?php echo $this->config->item('base_url');?>style/js/myapp.js" type="text/javascript"></script>
        <!-- AdminLTE App -->
        <script src="<?php echo $this->config->item('base_url');?>style/js/AdminLTE/app.js" type="text/javascript"></script>
        <script type="text/javascript">
                $(function() {

                    $('#myModal').on('hidden.bs.modal', function (e) {
                        $(this).removeData('bs.modal');
                    })
		    
        	});
        </script>
        

    </body>
</html>