<div class="modal-dialog">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			<h4 class="modal-title">Form Transaksi</h4>
		</div>
		<div class="modal-body">
			<form id="trxform" role="form">
                <div class="form-group">
					<label for="tanggal">Tanggal</label>
					<input type="text" class="form-control" id="tanggal" name="tanggal" maxlength="10" value="<?php echo (isset($trx) ? $trx['tanggal'] : '');?>" tanggalan>
				</div>
				<div class="form-group">
					<label for="keterangan">Keterangan</label>
					<input type="text" class="form-control" id="keterangan" name="keterangan" value="<?php echo (isset($trx)) ? $trx['keterangan'] : '';?>" maxlength="200">
				</div>
				<div class="form-group">
					<label for="matauang">Mata Uang</label>
					<select class="form-control" id="matauang" name="matauang">
						<?php
							foreach($matauang as $mu)
							{
								$selected = (isset($trx)) ? (($trx['matauang'] == $mu->id) ? 'selected=selected' : '') : '';
								echo "<option value=\"".$mu->id."\" ".$selected.">".$mu->kode."</option>";
							}
						?>
					</select>
				</div>
				<div class="form-group">
					<label for="nominal">Nominal</label>
					<input type="text" class="form-control" id="nominal" name="nominal" value="<?php echo (isset($trx)) ? $trx['nominal'] : '';?>" maxlength="10" data-mask data-inputmask="'alias': 'decimal'">
				</div>
				<div class="form-group">
					<label for="debetkredit">Debet / Kredit</label>
					<select class="form-control" id="debetkredit" name="debetkredit">
						<?php
							$deb = $this->config->item('trx_code');
							foreach($deb as $id => $val)
							{
								$selected = (isset($trx)) ? (($trx['debetkredit'] == $id) ? 'selected=selected' : '') : '';
								echo "<option value=\"".$id."\" ".$selected.">".$val."</option>";
							}
						?>
					</select>
				</div>
				<div class="form-group">
					<label for="remark">Keterangan Tambahan</label>
					<textarea class="form-control" id="remark" name="remark"><?php echo (isset($trx)) ? $trx['remark'] : '';?></textarea>
				</div>
				<input type="hidden" id="id" name="id" value="<?php echo (isset($trx)) ? $trx['id'] : '';?>">
				<input type="hidden" id="mode" name="mode" value="<?php echo $mode;?>">
			</form>
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Tutup</button>
			<button type="button" class="btn btn-primary btn-sm" id="TrxBtn">Simpan</button>
		</div>
	</div><!-- /.modal-content -->
</div><!-- /.modal-dialog -->
<script type="text/javascript">
	$(function() {
		$("[data-mask]").inputmask();
		$("[tanggalan]").datepicker({
                "dateFormat" : 'yy-mm-dd'
        });
		$("#TrxBtn").click(function() {
			var element = $(this);
			var tanggal = $("#tanggal").val();
			var keterangan = $("#keterangan").val();
			var nominal = $("#nominal").val();
                
			if(tanggal == '' || tanggal == '0000-00-00' || keterangan == '' || nominal == '')
			{
				alert("Tolong isi tanggal, keterangan dan nominal");
			}else{
				$.ajax({
				type: "POST",
				url: "<?php echo $this->config->item('base_url')?>transaksi/form_process/",
				data: $('#trxform').serialize(),
				cache: false,
				success: function(html){
							if (html == '1') {
								$("#ppGrid").html('Load..').load('<?php echo $this->config->item('base_url')?>transaksi/grid');
								$("#alertContent").html('Saved');
								$("#alertInfo").show();
								$('#myModal').modal('hide');
							}else{
								alert(html);
							}
                        }
				});
			}
			
			return false;
		});
	});
</script>