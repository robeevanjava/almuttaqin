<div class="modal-dialog">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			<h4 class="modal-title">Form Hapus Laporan</h4>
		</div>
		<div class="modal-body">
			<div class="callout callout-danger">
				<h4>Warning!</h4>
				<p>Yakin akan menghapus laporan ini? Data yang dihapus tidak dapat dikembalikan/</p>
			</div>
			<dl class="dl-horizontal">
				<dt>Mulai</dt>
				<dd><?php echo $lap['start']?></dd>
				<dt>Sampai</dt>
				<dd><?php echo $lap['finish']?></dd>
			</dl>
			<form id="lapdelform" role="form">
				<input type="hidden" id="id" name="id" value="<?php echo (isset($lap)) ? $lap['id'] : '';?>">
			</form>
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Tutup</button>
			<button type="button" class="btn btn-danger btn-sm" id="LapDelBtn">Hapus</button>
		</div>
	</div><!-- /.modal-content -->
</div><!-- /.modal-dialog -->
<script type="text/javascript">
	$(function() {
		$("[data-mask]").inputmask();

		$("#LapDelBtn").click(function() {
			var element = $(this);
			
			$.ajax({
			type: "POST",
			url: "<?php echo $this->config->item('base_url')?>laporan/del_process/",
			data: $('#lapdelform').serialize(),
			cache: false,
			success: function(html){
						if (html == '1') {
							$("#ppGrid").html('Load..').load('<?php echo $this->config->item('base_url')?>laporan/grid');
							$("#alertContent").html('Terhapus');
							$("#alertInfo").show();
							$('#myModal').modal('hide');
						}else{
							alert(html);
						}
                    }
			});
			
			return false;
		});
	});
</script>