<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Laporan_model extends CI_Model {

	function __construct()
	{
		parent::__construct();
	}

	public function grid($page = 1, $keyword)
	{
		$offset = 30;
		$limit = ($page - 1) * $offset;
		$keyword = $this->db->escape('%'.$keyword.'%');

		$qry_1 = $this->db->query("SELECT COUNT(*) as JML
										FROM jumatan
										INNER JOIN users ON jumatan.operator = users.id
										WHERE jumatan.imam LIKE $keyword OR jumatan.khotib1 LIKE $keyword OR jumatan.khotib2 LIKE $keyword 
											OR jumatan.bilal LIKE $keyword OR jumatan.protokol LIKE $keyword OR jumatan.start LIKE $keyword
											OR jumatan.finish LIKE $keyword");
		$count = $qry_1->row_array();
		$result['page'] = $page;
		$result['count'] = $count['JML'];
		$result['totalpage'] = ceil($count['JML'] / $offset);

		$qry = $this->db->query("SELECT jumatan.*, users.nama
									FROM jumatan
										INNER JOIN users ON jumatan.operator = users.id
										WHERE jumatan.imam LIKE $keyword OR jumatan.khotib1 LIKE $keyword OR jumatan.khotib2 LIKE $keyword 
											OR jumatan.bilal LIKE $keyword OR jumatan.protokol LIKE $keyword OR jumatan.start LIKE $keyword
											OR jumatan.finish LIKE $keyword
									ORDER BY jumatan.id DESC
									LIMIT $limit, $offset");

		$result['list'] = $qry->result();
		$this->db->close();
		return $result;

	}

	public function load_lap($id)
	{
		$id = $this->db->escape($id);
		return $this->db->query("SELECT * FROM jumatan WHERE MD5(id) = $id")->row_array();
	}

	public function add_process($data)
	{
		$this->db->trans_start();
		$this->db->insert('jumatan',$data);
		$id = $this->db->insert_id();
		$this->db->trans_complete();

		$return = $this->db->trans_status();

		$this->db->close();
		return $return;
	}

	public function update_process($data)
	{
		$this->db->trans_start();
		$this->db->where('id',$data['id']);
		unset($data['id']);
		$this->db->update('jumatan',$data);
		$this->db->trans_complete();

		$return = $this->db->trans_status();

		return $return;
	}
	public function del_process($id)
	{
		$id = $this->db->escape($id);
		$this->db->query("DELETE FROM jumatan WHERE MD5(id) = $id");
		return TRUE;
	}

	public function distinctMatauang()
	{
		return $this->db->query("SELECT DISTINCT matauang FROM transaksi")->result();
	}

	public function sumDebetBefore($date, $matauang)
	{
		$date = $this->db->escape($date);
		$matauang = $this->db->escape($matauang);

		$res = $this->db->query("SELECT SUM(nominal) as debet FROM transaksi WHERE tanggal <= $date AND matauang = $matauang AND debetkredit = 1")->row_array();
		return $res['debet'];
	}

	public function sumKreditBefore($date, $matauang)
	{
		$date = $this->db->escape($date);
		$matauang = $this->db->escape($matauang);

		$res =  $this->db->query("SELECT SUM(nominal) as kredit FROM transaksi WHERE tanggal <= $date AND matauang = $matauang AND debetkredit = 2")->row_array();
		return $res['kredit'];
	}

	public function debetWith($start, $finish)
	{
		$start = $this->db->escape($start);
		$finish = $this->db->escape($finish);
		return $this->db->query("SELECT transaksi.*, matauang.kode FROM transaksi INNER JOIN matauang ON transaksi.matauang = matauang.id WHERE tanggal > $start AND tanggal <= $finish AND debetkredit = 1")->result();
	}

	public function kreditWith($start, $finish)
	{
		$start = $this->db->escape($start);
		$finish = $this->db->escape($finish);
		return $this->db->query("SELECT transaksi.*, matauang.kode FROM transaksi INNER JOIN matauang ON transaksi.matauang = matauang.id WHERE tanggal > $start AND tanggal <= $finish AND debetkredit = 2")->result();
	}

	public function sumDebetWith($start, $finish, $matauang)
	{
		$start = $this->db->escape($start);
		$finish = $this->db->escape($finish);
		$matauang = $this->db->escape($matauang);

		$res = $this->db->query("SELECT SUM(nominal) as debet FROM transaksi WHERE tanggal > $start AND tanggal <= $finish AND matauang = $matauang AND debetkredit = 1")->row_array();
		return $res['debet'];
	}

	public function sumKreditWith($start, $finish, $matauang)
	{
		$start = $this->db->escape($start);
		$finish = $this->db->escape($finish);
		$matauang = $this->db->escape($matauang);

		$res =  $this->db->query("SELECT SUM(nominal) as kredit FROM transaksi WHERE tanggal > $start AND tanggal <= $finish AND matauang = $matauang AND debetkredit = 2")->row_array();
		return $res['kredit'];
	}

}