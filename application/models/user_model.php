<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class User_model extends CI_Model {

	function __construct()
	{
	    parent::__construct();
	}
    
	function check_credentials()
	{
		$user_name = $this->db->escape($_POST['user_name']);
		$user_pass = $this->db->escape( md5($_POST['user_pass']) );
		
		$sql = "SELECT * FROM users WHERE username = $user_name AND userpass = $user_pass ";
		$qry = $this->db->query($sql);
		
		return $qry->num_rows();
	}
	
	function load_userdata($username)
	{
		$username = $this->db->escape($username);
		$qry = $this->db->query("SELECT * FROM users WHERE username = $username");
		return $qry->row_array();
	}
	
	function reset_password ($username, $password)
	{
		$username = $this->db->escape($username);
		$password = $this->db->escape(md5($password));
		$this->db->query("UPDATE users SET userpass = $password WHERE username = $username");
		return True;
	}
	
	public function grid_load($page = 1, $keyword)
	{
		$offset = 30;
		$limit = ($page - 1) * $offset;
		$keyword = $this->db->escape('%'.$keyword.'%');
		
		$qry_1 = $this->db->query("
							SELECT COUNT(*) as JML
								FROM users
								INNER JOIN groups
									ON users.usergroup = groups.id
								WHERE 
									groups.nama LIKE $keyword OR users.username LIKE $keyword
									;");
		$count = $qry_1->row_array();
		$result['page'] = $page;
		$result['count'] = $count['JML'];
		$result['totalpage'] = ceil($count['JML'] / $offset);
		
		$qry = $this->db->query("
							SELECT sdm_pegawai.*, sdm_group.groupname
								FROM users
								INNER JOIN groups
									ON users.usergroup = groups.id
								WHERE 
									groups.nama LIKE $keyword OR users.username LIKE $keyword
								ORDER BY users.id
								LIMIT $limit, $offset");
		
		$result['list'] = $qry->result();
		$this->db->close();
		return $result;
	}
	
	function load_user_md5($usrid)
	{
		$usrid = $this->db->escape($usrid);
		$qry = $this->db->query("SELECT * FROM users WHERE MD5(id) = $usrid");
		return $qry->row_array();
	}
	
	public function update_process()
	{
	    $data = $_POST;
	    if($_POST['userpass'] == '')
			unset($data['userpass']);
	    else
			$data['userpass'] = md5($data['userpass']);
	    unset($data['nip']);
	    
	    
	    $usr = $this->load_user_md5(md5($data['id']));
	    if ($usr['username'] == $data['username'])
	    {
			unset($data['username']);
	    }
	    else{
			$usernum = $this->load_user_except($usr['username'], $data['username']);
			if ($usernum != 0)
			{
			    return 'Username is exist';
			}
	    }
	    $this->db->where('id',$data['id']);
	    unset($data['id']);
	    $this->db->update('users',$data);
	    
	    $this->db->close();
	    return TRUE;
	    
	}
	
	function load_user_except($old, $new)
	{
		$new = $this->db->escape($new);
		$old = $this->db->escape($old);
		$qry = $this->db->query("SELECT * FROM users WHERE `username` = $new AND `username` != $old");
		return $qry->num_rows();
	}
	//------------------------------------------------------------------------------------------------------
	
	
	
}