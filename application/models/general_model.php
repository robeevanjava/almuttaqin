<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class General_model extends CI_Model {

    function __construct()
    {
        parent::__construct();
    }
    
    public function userdata($id)
    {
        $id = $this->db->escape($id);
        return $this->db->query("SELECT * FROM users WHERE id = $id")->row_array();
    }

    public function matauang()
    {
        return $this->db->query("SELECT * FROM matauang ORDER BY id")->result();
    }

    public function get_matauang($id)
    {
        $id = $this->db->escape($id);
        return $this->db->query("SELECT * FROM matauang WHERE id = $id")->row_array();
    }
    public function load_alluser()
    {
        return $this->db->query("SELECT * FROM users ORDER BY id")->result();
    }
}