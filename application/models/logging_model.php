<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Logging_model extends CI_Model {

    function __construct()
    {
        parent::__construct();
		$this->load->library('generaltools');
    }
    
    public function SaveLog($module, $remark)
    {
		if($this->session->userdata('masjid_logged') === TRUE)
		{
			$username = $this->db->escape($this->session->userdata('masjid_username'));
			$userqry = $this->db->query("SELECT * FROM users WHERE username = $username");
			$userdata = $userqry->row_array();
			
			$userid = $this->db->escape($userdata['id']);
		}
		else
			$userid = $this->db->escape(0);

		$module = $this->db->escape($module);
		$remark = $this->db->escape($remark);
		$ipaddr = $this->db->escape($this->generaltools->getipaddress());
		$this->db->query("INSERT INTO `userlog` VALUES (NULL, $module, $remark, $ipaddr, $userid, NOW())");
		$this->db->close();
		return True;
		
    }
}