<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Transaksi_model extends CI_Model {

	function __construct()
	{
		parent::__construct();
	}

	public function grid($page = 1, $keyword)
	{
		$offset = 30;
		$limit = ($page - 1) * $offset;
		$keyword = $this->db->escape('%'.$keyword.'%');

		$qry_1 = $this->db->query("SELECT COUNT(*) as JML
										FROM transaksi
										INNER JOIN matauang ON transaksi.matauang = matauang.id
										INNER JOIN users ON transaksi.operator = users.id
										WHERE transaksi.keterangan LIKE $keyword OR transaksi.nominal LIKE $keyword OR matauang.kode LIKE $keyword OR transaksi.tanggal LIKE $keyword");
		$count = $qry_1->row_array();
		$result['page'] = $page;
		$result['count'] = $count['JML'];
		$result['totalpage'] = ceil($count['JML'] / $offset);

		$qry = $this->db->query("SELECT transaksi.*, users.nama, matauang.kode
									FROM transaksi
											INNER JOIN matauang ON transaksi.matauang = matauang.id
											INNER JOIN users ON transaksi.operator = users.id
											WHERE transaksi.keterangan LIKE $keyword OR transaksi.nominal LIKE $keyword OR matauang.kode LIKE $keyword OR transaksi.tanggal LIKE $keyword
									ORDER BY transaksi.id DESC
									LIMIT $limit, $offset");

		$result['list'] = $qry->result();
		$this->db->close();
		return $result;

	}

	public function load_trx($id)
	{
		$id = $this->db->escape($id);
		return $this->db->query("SELECT transaksi.*, matauang.kode FROM transaksi INNER JOIN matauang ON transaksi.matauang = matauang.id WHERE MD5(transaksi.id) = $id")->row_array();
	}

	public function add_process($data)
	{
		$this->db->trans_start();
		$this->db->insert('transaksi',$data);
		$id = $this->db->insert_id();
		$this->db->trans_complete();

		$return = $this->db->trans_status();

		$this->db->close();
		return $return;
	}

	public function update_process($data)
	{
		$this->db->trans_start();
		$this->db->where('id',$data['id']);
		unset($data['id']);
		$this->db->update('transaksi',$data);
		$this->db->trans_complete();

		$return = $this->db->trans_status();

		return $return;
	}
	public function del_process($id)
	{
		$id = $this->db->escape($id);
		$this->db->query("DELETE FROM transaksi WHERE MD5(id) = $id");
		return TRUE;
	}
}