<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Menu_model extends CI_Model {

    function __construct()
    {
        parent::__construct();
    }
    
	public function menu_load($parent)
	{
		$username = $this->db->escape($this->session->userdata('masjid_username'));
		$userqry = $this->db->query("SELECT * FROM users WHERE username = $username");
		$usergroup = $userqry->row_array();
				
		$group = $this->db->escape($usergroup['usergroup']);
		
		$parent = $this->db->escape($parent);
		$qry = $this->db->query("SELECT * FROM menu WHERE parent = $parent AND FIND_IN_SET($group, groups) <> 0 AND active = 1 ORDER BY `order`");
		$retval = $qry->result();
		$this->db->close();
		return $retval;
	}
	
	public function menu_childnum($id)
	{
		$username = $this->db->escape($this->session->userdata('masjid_username'));
		$userqry = $this->db->query("SELECT * FROM users WHERE username = $username");
		$usergroup = $userqry->row_array();
				
		$group = $this->db->escape($usergroup['usergroup']);
	
		$id = $this->db->escape($id);
		$qry = $this->db->query("SELECT * FROM menu WHERE parent = $id AND FIND_IN_SET($group, groups) <> 0 AND active = 1");
		$retval = $qry->num_rows();
		$this->db->close();
		return $retval;
	}
    
	function menu_builder($ac_par, $ac_child)
	{
		$menu = "";
		$menu_top = $this->menu_load(0);
		foreach ($menu_top as $row)
		{
			$active = $active_ch = "";
			$nummenu = $this->menu_childnum($row->id);
			if ($nummenu == 0){
				if($ac_par == $row->id)
					$active = ' class="active"';
				$menu .= "<li".$active."><a href=\"".base_url().$row->url."\">".$row->title."</a></li>";
			}else
			{
				if($ac_par == $row->id)
					$active = ' active';
				$menu_child = $this->menu_load($row->id);
				$menu .= '	<li class="treeview'.$active.'">
								<a href="#">'.$row->title.' <i class="fa fa-angle-left pull-right"></i></a>
								
								<ul class="treeview-menu">';
				foreach($menu_child as $child)
				{
					$active_ch = '';
					if($ac_child == $child->id)
						$active_ch = ' class="active"';
					$menu .= '<li'.$active_ch.'><a href="'.base_url().$child->url.'"> <i class="fa fa-angle-double-right"></i>'.$child->title.'</a></li>';
				}
				$menu .= '</ul></li>';
			}
		}
		
		return $menu;
	}
	
	public function menu_access()
	{
		$username = $this->db->escape($this->session->userdata('masjid_username'));
		$userqry = $this->db->query("SELECT * FROM users WHERE username = $username");
		$usergroup = $userqry->row_array();
				
		$group = $this->db->escape($usergroup['usergroup']);
		$query = $this->db->query("SELECT id FROM menu WHERE FIND_IN_SET($group, groups) <> 0");
		$val = array();
		foreach( $query->result() as $row)
		{
			array_push($val, $row->id);
		}
		$this->db->close();
		return $val;
	}
	
	public function grid_load($page = 1, $keyword)
	{
		$offset = 30;
		$limit = ($page - 1) * $offset;
		$keyword = $this->db->escape('%'.$keyword.'%');
		
		$qry_1 = $this->db->query("
							SELECT COUNT(*) as JML
								FROM menu
									;");
		$count = $qry_1->row_array();
		$result['page'] = $page;
		$result['count'] = $count['JML'];
		$result['totalpage'] = ceil($count['JML'] / $offset);
		
		$qry = $this->db->query("
							SELECT *
								FROM menu
								ORDER BY `parent`, `order`
								LIMIT $limit, $offset");
		
		$result['list'] = $qry->result();
		$this->db->close();
		return $result;
	}
	
	function menu_grid()
	{
		$menu = array();
		$menu_top = $this->menu_load(0);
		$i = 0;
		foreach ($menu_top as $row)
		{
			
			$nummenu = $this->menu_childnum($row->id);
			if ($nummenu == 0){				
				$menu[$i]['parent'] = $row;
				$menu[$i]['child'] = 0;
			}else
			{				
				$menu_child = $this->menu_load($row->id);
				$menu[$i]['parent'] = $row;
				
				$ch = 0;
				foreach($menu_child as $child)
				{					
					$menu[$i]['child'][$ch] = $child;
					$ch ++;
				}
				
			}
			
			$i++;
		}
		
		return $menu;
	}
	
	public function load_menu($id)
	{
	    $id = $this->db->escape($id);
	    return $this->db->query("SELECT * FROM menu WHERE MD5(id) = $id")->row_array();
	}
	
	public function update($data)
	{
	    $this->db->where('id',$data['id']);
	    unset($data['id']);
	    $this->db->update('menu',$data);
	    
	    $this->db->close();
	    return TRUE;
	}
	
	
}