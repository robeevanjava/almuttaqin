<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
    $config['appname'] = 'e-Laporan';
    $config['appversion'] = "1.0";
    $config['title'] = "Sistem Pelaporan Transaksi Masjid";
    $config['tagline'] = "-";
    $config['company'] = "Masjid Al-Muttaqin";
    $config['keyword'] = "sistem pelaporan transasksi masjid online";
    $config['trx_code'] = array(
    						1 => 'Debet',
    						2 => 'Kredit',
    					);

    $config['usergroup'] = array(
                            1 => 'Administrators',
                            2 => 'Users',
                        );
    $config['bulan_ind'] = array(
                            1 => 'Januari',
                            2 => 'Februari',
                            3 => 'Maret',
                            4 => 'April',
                            5 => 'Mei',
                            6 => 'Juni',
                            7 => 'Juli',
                            8 => 'Agustus',
                            9 => 'September',
                            10 => 'Oktober',
                            11 => 'November',
                            12 => 'Desember'
                        );

    $config['hari_ind'] = array(
                            1 => 'Senin',
                            2 => 'Selasa',
                            3 => 'Rabu',
                            4 => 'Kamis',
                            5 => 'Jumat',
                            6 => 'Sabtu',
                            7 => 'Minggu',
                        );