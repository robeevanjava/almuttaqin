<?php
    
    
    function get_kelurahan($data)
    {
        if ($data == 0 or $data == 1)
            return '';
        else
        {
            $CI =& get_instance();
            $CI->load->model('General_model');
            $kel = $CI->General_model->kelurahan($data);
            return $kel['nama'];
        }
        
    }
    
    function get_kecamatan($data)
    {
        if ($data == 0 or $data == 1)
            return '';
        else
        {
            $CI =& get_instance();
            $CI->load->model('General_model');
            $kel = $CI->General_model->kecamatan($data);
            return $kel['nama'];
        }
        
    }
?>